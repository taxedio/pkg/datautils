package datautils

import "testing"

func TestSameBool(t *testing.T) {
	type args struct {
		s []bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Test 1, all true",
			args{
				s: []bool{true, true, true, true, true, true, true},
			},
			true,
		},
		{
			"Test 2, all false",
			args{
				s: []bool{false, false, false, false, false, false, false},
			},
			true,
		},
		{
			"Test 3, all true and 1 false",
			args{
				s: []bool{true, true, true, true, true, true, false},
			},
			false,
		},
		{
			"Test 4, all false and 1 true",
			args{
				s: []bool{false, false, false, false, false, false, true},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SameBool(tt.args.s); got != tt.want {
				t.Errorf("SameBool() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContainsFalse(t *testing.T) {
	type args struct {
		b []bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Test 1 - returns true",
			args{b: []bool{true, false}},
			true,
		},
		{
			"Test 1 - returns false",
			args{b: []bool{true, true}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsFalse(tt.args.b); got != tt.want {
				t.Errorf("ContainsFalse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContainsTrue(t *testing.T) {
	type args struct {
		b []bool
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Test 1 - returns true",
			args{b: []bool{true, false}},
			true,
		},
		{
			"Test 1 - returns false",
			args{b: []bool{false, false}},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ContainsTrue(tt.args.b); got != tt.want {
				t.Errorf("ContainsTrue() = %v, want %v", got, tt.want)
			}
		})
	}
}
