package datautils

import (
	"fmt"
	"regexp"
	"strings"
)

// JSON binding cleansing
// can't look forward so must split based on start and finish
func CleanseJSONBindError(s string) *string {
	const (
		jsonStringStart   = "json: cannot unmarshal string into Go struct field "
		jsonNumberStart   = "json: cannot unmarshal number into Go struct field "
		jsonBoolStart     = "json: cannot unmarshal bool into Go struct field "
		jsonObjStart      = "json: cannot unmarshal object into Go struct field "
		jsonArrStart      = "json: cannot unmarshal array into Go struct field "
		regExpStructStart = "^[a-zA-Z]*."
		regExpEnd         = ` of type [a-zA-Z\d]*$`
	)
	var (
		actualExp string
	)
	// remove beginning
	if strings.Contains(s, jsonStringStart) {
		s = strings.Replace(s, jsonStringStart, "", -1)
		actualExp = "string"
	}
	if strings.Contains(s, jsonNumberStart) {
		s = strings.Replace(s, jsonNumberStart, "", -1)
		actualExp = "number"
	}
	if strings.Contains(s, jsonBoolStart) {
		s = strings.Replace(s, jsonBoolStart, "", -1)
		actualExp = "boolean"
	}
	if strings.Contains(s, jsonObjStart) {
		s = strings.Replace(s, jsonObjStart, "", -1)
		actualExp = "object"
	}
	if strings.Contains(s, jsonArrStart) {
		s = strings.Replace(s, jsonArrStart, "", -1)
		actualExp = "array"
	}

	// find and remove struct beginning
	mid := regexp.MustCompile(regExpStructStart)
	s = strings.Replace(s, strings.Join(mid.FindStringSubmatch(s), ""), "", -1)

	// remove
	re := regexp.MustCompile(regExpEnd)
	// find end string
	foundExp := strings.Join(re.FindStringSubmatch(s), "")
	foundExp = strings.Replace(foundExp, " of type ", "", -1)
	// remove end string
	s = strings.Replace(s, " of type "+foundExp, "", -1)
	s = fmt.Sprintf("json member %v sent as %v but should be %v", s, actualExp, foundExp)
	return &s
}
