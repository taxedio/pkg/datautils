package datautils

import (
	"reflect"
	"testing"

	"github.com/taxedio/overthere"
)

func TestContains(t *testing.T) {
	type args struct {
		s   []string
		str string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test Contains 1 - true", args{s: []string{"a", "b", "c", "d"}, str: "a"}, true},
		{"test Contains 2 - false", args{s: []string{"a", "b", "c", "d"}, str: "e"}, false},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Contains(tt.args.s, tt.args.str); got != tt.want {
				t.Errorf("Contains() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestParseNum(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want *[]int
	}{
		{"test 1", args{s: "test 1"}, &[]int{1}},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ParseNum(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ParseNum() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStrConcat(t *testing.T) {
	type args struct {
		s  string
		ns string
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			"Test StrConcat 1",
			args{s: "", ns: ""},
			"",
		},
		{
			"Test StrConcat 2",
			args{s: "hour is blank", ns: "hour is blank"},
			"hour is blank | hour is blank",
		},
		{
			"Test StrConcat 3",
			args{s: "hour is blank | hour is blank", ns: "hour is blank"},
			"hour is blank | hour is blank | hour is blank",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StrConcat(tt.args.s, tt.args.ns); got != tt.want {
				t.Errorf("StrConcat() = %#v, want %v#", got, tt.want)
			}
		})
	}
}

func TestCleanseISOString(t *testing.T) {
	type args struct {
		s string
		i int
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		{"Test 1 - blank alpha2", args{"", 2}, nil},
		{"Test 2 - blank alpha3", args{"", 2}, nil},
		{"Test 3 - short alpha2", args{"g", 2}, nil},
		{"Test 4 - long alpha2", args{"gbr", 2}, nil},
		{"Test 5 - short alpha3", args{"g", 3}, nil},
		{"Test 6 - long alpha3", args{"gbrr", 3}, nil},
		{"Test 7 - okay alpha2 [gbr]", args{"gb", 2}, overthere.StrPointer("gb")},
		{"Test 8 - okay alpha3 [gbr]", args{"gbr", 3}, overthere.StrPointer("gbr")},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CleanseISOString(tt.args.s, tt.args.i); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CleanseIsoString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestDifferingStrings(t *testing.T) {
	type args struct {
		s []*string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{"test a", args{s: []*string{overthere.StrPointer("a"), overthere.StrPointer("a")}}, false},
		{"test a b", args{s: []*string{overthere.StrPointer("a"), overthere.StrPointer("b")}}, true},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := DifferingStrings(tt.args.s); got != tt.want {
				t.Errorf("DifferingStrings() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_convertDia(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := ConvertDia(tt.args.s); got != tt.want {
				t.Errorf("convertDia() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestAlphaNumericOnly(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		{
			"test 1 - pass",
			args{s: ""},
			overthere.StrPointer(""),
		},
		{
			"test 2 - pass",
			args{s: "ê"},
			overthere.StrPointer("e"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := AlphaNumericOnly(tt.args.s)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("AlphaNumericOnly() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStringArrayMatch(t *testing.T) {
	type args struct {
		got []string
		exp []string
	}
	tests := []struct {
		name string
		args args
		want bool
	}{
		{
			"Test 1 - true",
			args{
				got: []string{"US", "GB"},
				exp: []string{"GB", "US"},
			},
			true,
		},
		{
			"Test 2 - false",
			args{
				got: []string{"US", "GB"},
				exp: []string{"", "US"},
			},
			false,
		},
		{
			"Test 3 - false len",
			args{
				got: []string{"US", "GB"},
				exp: []string{"US"},
			},
			false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := StringArrayMatch(tt.args.got, tt.args.exp); got != tt.want {
				t.Errorf("StringArrayMatch() = %v, want %v", got, tt.want)
			}
		})
	}
}
