package datautils

import (
	"reflect"
	"testing"

	"github.com/taxedio/overthere"
)

func TestCleanseJSONBindError(t *testing.T) {
	type args struct {
		s string
	}
	tests := []struct {
		name string
		args args
		want *string
	}{
		{
			"test 1 ",
			args{
				s: "json: cannot unmarshal string into Go struct field CEFI.entities.entity_financial_information.related_revenue of type int64",
			},
			overthere.StrPointer("json member entities.entity_financial_information.related_revenue sent as string but should be int64"),
		},
		{
			"test 2 ",
			args{
				s: "json: cannot unmarshal number into Go struct field CEFI.entities.entity_financial_information.currency_code of type string",
			},
			overthere.StrPointer("json member entities.entity_financial_information.currency_code sent as number but should be string"),
		},
		{
			"test 3 ",
			args{
				s: "json: cannot unmarshal bool into Go struct field CEFI.entities.entity_financial_information.related_revenue of type int64",
			},
			overthere.StrPointer("json member entities.entity_financial_information.related_revenue sent as boolean but should be int64"),
		},
		{
			"test 4 ",
			args{
				s: "json: cannot unmarshal object into Go struct field CEFI.entities.entity_financial_information.related_revenue of type int64",
			},
			overthere.StrPointer("json member entities.entity_financial_information.related_revenue sent as object but should be int64"),
		},
		{
			"test 5 ",
			args{
				s: "json: cannot unmarshal array into Go struct field CEFI.entities.entity_financial_information.related_revenue of type int64",
			},
			overthere.StrPointer("json member entities.entity_financial_information.related_revenue sent as array but should be int64"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := CleanseJSONBindError(tt.args.s); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("CleanseJSONBindError() = %v, want %v", got, tt.want)
			}
		})
	}
}
