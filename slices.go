package datautils

// removes head of slices
func RemoveHeadSlicedString(s [][]string, start int, end *int) [][]string {
	if end == nil {
		return s[start:]
	} else {
		return s[start:*end]
	}
}

