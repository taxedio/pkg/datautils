package datautils

import (
	"fmt"
	"strings"
	"unicode"

	"github.com/taxedio/overthere"
	"golang.org/x/text/runes"
	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

// checks if string slice contains string, returns true if found
func Contains(s []string, str string) bool {
	for _, v := range s {
		if v == str {
			return true
		}
	}

	return false
}

// returns numeric values
// e.g. £12,000 --> 12000
func ParseNum(s string) *[]int {
	nLen := 0
	for i := 0; i < len(s); i++ {
		if b := s[i]; '0' <= b && b <= '9' {
			nLen++
		}
	}
	var n = make([]int, 0, nLen)
	for i := 0; i < len(s); i++ {
		if b := s[i]; '0' <= b && b <= '9' {
			n = append(n, int(b)-'0')
		}
	}
	return &n
}

// StrConcat, concats two string fields with separator "|"
func StrConcat(s string, ns string) string {
	if s == "" {
		s = ns
	} else {
		s += fmt.Sprintf(" | %v", ns)
	}
	return s
}

// Turns string into requested format, lower. returns same string in lower and trimspace
func CleanseISOString(s string, i int) *string {
	if s == "" {
		return nil
	}
	s = strings.TrimSpace(strings.ToLower(s))
	if len(s) < i {
		return nil
	}
	if len(s) > i {
		return nil
	}
	return &s
}

// Returns true if any values are duplicates
func DifferingStrings(s []*string) bool {
	dupeStrings := make(map[string]int)
	for _, item := range s {
		_, exist := dupeStrings[*item]
		if exist {
			dupeStrings[*item] += 1
		} else {
			dupeStrings[*item] = 1
		}
	}
	return len(dupeStrings) > 1
}

// Removes Diacritics from submitted string
func ConvertDia(s string) *string {
	t := transform.Chain(norm.NFD, runes.Remove(runes.In(unicode.Mn)), norm.NFC)
	s, _, _ = transform.String(t, s)
	return &s
}

// Strips non-alphanumeric values
func AlphaNumericOnly(s string) *string {
	s = *ConvertDia(s)
	var result strings.Builder
	for i := 0; i < len(s); i++ {
		b := s[i]
		if ('a' <= b && b <= 'z') ||
			('A' <= b && b <= 'Z') ||
			('0' <= b && b <= '9') ||
			b == ' ' {
			result.WriteByte(b)
		}
	}
	return overthere.StrPointer(result.String())
}

// Returns true if both string slices match (disregards order)
func StringArrayMatch(got []string, exp []string) bool {
	allfound := []bool{}
	for _, first := range got {
		found := false
		for _, second := range exp {
			if first == second {
				found = true
				break
			}
		}
		allfound = append(allfound, found)
	}
	if len(got) != len(exp) { // something missing or unexpected
		allfound = append(allfound, false)
	}
	if SameBool(allfound) && !ContainsFalse(allfound) {
		return true
	}
	return false
}
