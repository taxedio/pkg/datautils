package datautils

// returns true if all values in a []bool match
func SameBool(s []bool) bool {
	for i := 0; i < len(s); i++ {
		if s[i] != s[0] {
			return false
		}
	}
	return true
}

// checks if string slice contains string, returns true if found
func ContainsFalse(b []bool) bool {
	for _, v := range b {
		if !v {
			return true
		}
	}
	return false
}

// checks if string slice contains string, returns true if found
func ContainsTrue(b []bool) bool {
	for _, v := range b {
		if v {
			return true
		}
	}
	return false
}
