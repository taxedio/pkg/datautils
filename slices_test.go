package datautils

import (
	"reflect"
	"testing"

	"github.com/taxedio/overthere"
)

func TestRemoveHeadSlicedString(t *testing.T) {
	type args struct {
		s     [][]string
		start int
		end   *int
	}
	tests := []struct {
		name string
		args args
		want [][]string
	}{
		{
			"Test 1 - remove headers",
			args{
				s: [][]string{
					{
						"Please select entity that has additional id",
						"automatic field, please do not modify (modify on entities tab if required)",
						"automatic field, please do not modify (modify on entities tab if required)",
						"",
						"",
						"",
					},
					{
						"LIST",
						"AUTOMATIC",
						"AUTOMATIC",
						"TEXT",
						"LIST",
						"",
					},
					{
						"UUID",
						"Name",
						"Tax Jurisdiction",
						"Identificaiton Type",
						"Identificaiton Number",
						"Issued By",
					},
					{
						"1",
						"taxed.io limited",
						"UK",
						"CORPTAX",
						"ABC123CT",
						"UK",
					},
				},
				start: 2,
				end:   overthere.IntPointer(4),
			},
			[][]string{
				{
					"UUID",
					"Name",
					"Tax Jurisdiction",
					"Identificaiton Type",
					"Identificaiton Number",
					"Issued By",
				},
				{
					"1",
					"taxed.io limited",
					"UK",
					"CORPTAX",
					"ABC123CT",
					"UK",
				},
			},
		},
		{
			"Test 2 - remove headers no end",
			args{
				s: [][]string{
					{
						"Please select entity that has additional id",
						"automatic field, please do not modify (modify on entities tab if required)",
						"automatic field, please do not modify (modify on entities tab if required)",
						"",
						"",
						"",
					},
					{
						"LIST",
						"AUTOMATIC",
						"AUTOMATIC",
						"TEXT",
						"LIST",
						"",
					},
					{
						"UUID",
						"Name",
						"Tax Jurisdiction",
						"Identificaiton Type",
						"Identificaiton Number",
						"Issued By",
					},
					{
						"1",
						"taxed.io limited",
						"UK",
						"CORPTAX",
						"ABC123CT",
						"UK",
					},
				},
				start: 2,
				end:   nil,
			},
			[][]string{
				{
					"UUID",
					"Name",
					"Tax Jurisdiction",
					"Identificaiton Type",
					"Identificaiton Number",
					"Issued By",
				},
				{
					"1",
					"taxed.io limited",
					"UK",
					"CORPTAX",
					"ABC123CT",
					"UK",
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := RemoveHeadSlicedString(tt.args.s, tt.args.start, tt.args.end); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("RemoveHeadSlicedString() = %v, want %v", got, tt.want)
			}
		})
	}
}
